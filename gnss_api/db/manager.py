from pydantic import BaseModel, ConfigDict
from sqlmodel import Session
from sqlalchemy.engine import Engine
from pathlib import Path
import polars as pl
from sqlalchemy.orm import lazyload
from sqlmodel import select, col, or_, and_
from gnss_api.db.models import Machine,EpochRegister
from gnss_api.db.functions import get_engine
from typing import List, Any

DataFrame = pl.DataFrame | List[Any]

class DBManager(BaseModel):
    model_config = ConfigDict(arbitrary_types_allowed=True)
    engine:Engine
    transaction:bool=False

    @classmethod
    def create(cls, params):
        engine = get_engine(params, False)
        transaction = False
        #breakpoint()
        return cls(engine=engine, transaction=transaction)

    def new_machine(self, **params)->Machine:
        # test: ok
        with Session(self.engine) as session:
            machine = Machine(**params)
            session.add(machine)
            session.commit()
            session.refresh(machine)
            return machine

    def new_register(self, **params)->EpochRegister:
        # test: ok
        with Session(self.engine) as session:
            register = EpochRegister(**params)
            session.add(register)
            session.commit()
            session.refresh(register)
            return register

    def get_machine(self, mid:int):
        with Session(self.engine) as session:
            stmt = select(Machine).where(Machine.id==mid)
            if self.transaction:
                self.statements.append(stmt)
            else:
                for machine in session.exec(stmt):
                    yield machine


    def get_machines(self):
        # test: ok
        with Session(self.engine) as session:
            stmt = select(Machine)
            if self.transaction:
                self.statements.append(stmt)
            else:
                for machine in session.exec(stmt):
                    yield machine

    def get_register(self, rid:int):
        with Session(self.engine) as session:
            stmt = select(EpochRegister).where(EpochRegister.id==rid)
            if self.transaction:
                self.statements.append(stmt)
            else:
                for register in session.exec(stmt):
                    yield register


    def get_registers(self, machine_id:int):
        # test: ok
        with Session(self.engine) as session:
            machines = select(Machine).where(Machine.id==machine_id)
            if self.transaction:
                self.statements.append(machines)
            else:
                for machine in session.execute(machines):
                    for register in machine.registers:
                        yield machine.id, register

    def get_last_register(self, machine_id:int)->EpochRegister:
        # test: ok
        with Session(self.engine) as session:
            machines = select(EpochRegister)\
                .where(EpochRegister.machine_id==machine_id)\
                .order_by(EpochRegister.timestamp.desc())
            if self.transaction:
                self.statements.append(machines)
            else:
                for i, machine in enumerate(session.exec(machines)):
                    yield machine
                    if i==0:
                        break


    def info(self, machine_id:int):
        with Session(self.engine) as session:
            machines = select(Machine).where(col("id")==machine_id)
            if self.transaction:
                self.statements.append(machines)
            else:
                for machine in machines:
                    print(machine)
                    machine.show()

    def model_params(self,machine_id:int, stage:int)->Path:
        with Session(self.engine) as session:
            machines = select(EpochRegister).where(
                EpochRegister.machine_id==machine_id, 
                EpochRegister.stage==stage)
            if self.transaction:
                self.statements.append(machines)
            else:
                for register in session.exe(machines):
                    yield register.stage_model


    def acc_loss(self,machine_id:int, stage:int, dataframe:bool=True)->DataFrame:
        with Session(self.engine) as session:
            registers = select(EpochRegister).where(
                EpochRegister.machine_id==machine_id, 
                EpochRegister.stage<=stage)
            if self.transaction:
                self.statements.append(registers)
            else:
                for results in self.execute([registers]):
                    dataset = [s.stage_data() for s in results]
                    if dataframe:
                        yield pl.DataFrame(dataset)
                    else:
                        yield dataset

    def transaction(self)->bool:
        self.transaction = not self.transaction

    def execute(self, statements):
        with Session(self.engine) as session:
            for statement in statements:
                result = session.exec(statement)
                session.commit()
                yield result

    def run_all(self):
        self.execute(self.statements)

    def clear(self):
        self.statements.clear()
