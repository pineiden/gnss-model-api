from sqlmodel import (
    Session, 
    create_engine)

from models import SQLModel
import toml
from pathlib import Path
import typer
from typing import Dict,Any
from functions import get_engine

"""
Read db params from toml file with:
[database]
dbname
username
password
dbhost
dbport
"""

app = typer.Typer()


@app.command()
def create(params:Path):
    if params.exists():
        txt = params.read_text()
        params = toml.loads(txt)
        engine = get_engine(params["database"],True)
        print("Engine :>", engine)
        SQLModel.metadata.create_all(engine)  
    else:
        print("Params path does not exist")


if __name__ == "__main__":
    app()
