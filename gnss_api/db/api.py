"""
Here the FastAPI implementation for Manager
"""


from typing import Dict, List, Any
from fastapi import FastAPI
from gnss_api.db.manager import DBManager
from gnss_api.db.models import Machine, EpochRegister

from pathlib import Path
import toml
from fastapi.responses import StreamingResponse
from fastapi.responses import ORJSONResponse
from datetime import datetime

app = FastAPI(default_response_class=ORJSONResponse)


base_path = Path(__file__).parent.parent.parent.parent

default_params = base_path / "db_params.toml"
params = toml.loads(default_params.read_text())
manager = DBManager.create(params["database"])

def get_manager():
    manager = DBManager.create(params["database"])
    try:
        yield manager
    finally:
        pass
        #manager.close()


@app.get("/ping")
async def ping():
    return {"message":"pong"}


@app.get("/machine")
async def get_machine(mid:int):
    result =  list(manager.get_machine(mid))
    return result


@app.get("/register")
async def get_register(rid:int):
    result =  list(manager.get_register(rid))
    return result


@app.get("/machines")
async def get_machines():
    result =  list(manager.get_machines())
    return result


@app.get("/registers")
async def get_registers(machine_id:int):
    result =  list(manager.get_registers(machine_id))
    return result


@app.get("/last_register")
async def last_register(machine_id:int):
    result =  list(manager.get_last_register(machine_id))
    return result



@app.get("/params")
async def get_params(machine_id:int, stage:int):
    result =  list(manager.model_params(machine_id, stage))
    return result


@app.get("/acc_loss")
async def get_acc_loss(machine_id:int, stage:int):
    result =  manager.acc_loss(machine_id, stage, False)
    return result

#######
"""
Now, the post requests

- post new machine
- post new register
"""
@app.post("/new_machine")
async def create_new_machine(params:Dict[str,Any]):
    result = manager.new_machine(**params)
    return result


@app.post("/new_register")
async def create_new_epoch_register(params:Dict[str,Any]):
    params["timestamp"] = datetime.fromisoformat(params["timestamp"])
    result = manager.new_register(**params)
    return result

