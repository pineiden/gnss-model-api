from sqlmodel import create_engine
from typing import Any

def get_engine(params:dict[str,Any], create:bool=False):
    engine_path = "postgresql+psycopg2://{username}:{password}@{dbhost}:{dbport}/{dbname}".format(**params)
    return create_engine(engine_path, echo=create)
