from typing import Optional,Dict,List,Any
from decimal import Decimal
from sqlmodel import (
    Field, 
    SQLModel,
    Relationship, 
    Session, 
    create_engine)
from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import JSONB
from datetime import datetime
from pathlib import Path
from rich import print
from typing import TypeVar
from pydantic.functional_validators import field_validator
from pydantic import ConfigDict

Loss = TypeVar("LossManager")
Accuracy = TypeVar("AccManager")

class Machine(SQLModel,table=True):
    """
    This instance register all the main parameters that define the
    model to train.
    """
    model_config = ConfigDict(arbitrary_types_allowed=True)

    id:Optional[int] = Field(
        default=None, 
        primary_key=True)
    name:str
    description:str
    timestamp:datetime=Field(
        index=True,
        default_factory=datetime.utcnow,
        nullable=False
    )
    edited: datetime = Field(
        default_factory=datetime.utcnow,
        nullable=False)
    hyperparameters:Dict[str,Any]=Field(
        default=dict, 
        sa_column=Column(JSONB))
    optimizer:str
    criterion:str
    epochs:int
    log_path:Path
    dataset_path:Path
    dataset_size:int
    ml_path:Path
    registers:List["EpochRegister"] = Relationship(
        back_populates='machine')


    def __repr__(self):
        return f"Machine(id={self.id}, model={self.ml_path})"


    def __str__(self):
        return f"Machine(id={self.id}, model={self.ml_path})"


    def show(self):
        for register in registers:
            print(register)

class EpochRegister(SQLModel,table=True):
    """
    This instance register every new epochs 
    and main results related to the model trained.
    """
    model_config = ConfigDict(arbitrary_types_allowed=True)

    id:Optional[int] = Field(
        default=None, 
        primary_key=True)
    machine_id:Optional[int] = Field(
        default=None, 
        foreign_key='machine.id')
    duration:float
    timestamp:datetime=Field(
        default_factory=datetime.utcnow,
        nullable=False)
    stage:int=Field(default=0)
    stage_model:Dict[str,Any]=Field(
        default=dict, 
        sa_column=Column(JSONB))
    loss:Loss=Field(
        default=dict, 
        sa_column=Column(JSONB))
    acc:Accuracy=Field(
        default=dict, 
        sa_column=Column(JSONB))

    machine:Optional[Machine] = Relationship(
        back_populates='registers')

    def __repr__(self):
        return f"""EpochRegister(
        id={self.id},
        duration={self.duration},
        acc='{self.acc}'
        loss='{self.loss}')"""

    def __str__(self):
        return f"""EpochRegister(
        id={self.id},
        duration={self.duration},
        acc='{self.acc}'
        loss='{self.loss}')"""

    @field_validator('loss')
    def val_acc(cls, val: Loss):
        # Used in order to store pydantic models as dicts
        return val.dict_loss()


    @field_validator('loss')
    def val_loss(cls, val: Accuracy):
        # Used in order to store pydantic models as dicts
        return val.dict_acc()


    def stage_data(self):
        loss = {f"loss_{k}":v for k,v in self.loss.items()}
        acc = {f"acc_{k}":v for k,v in self.acc.items()}

        return {
            "stage":self.stage,
            "ts":self.timestamp,
            **loss,
            **acc
        }
