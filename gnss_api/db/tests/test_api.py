from gnss_api.db.api import app
import pytest
from fastapi.testclient import TestClient
from httpx import AsyncClient
from pathlib import Path
from datetime import datetime

@pytest.mark.anyio
async def test_ping():
    async with AsyncClient(app=app, base_url='http://test') as ac:
        response = await ac.get("/ping")
    assert response.status_code==200
    assert response.json() == {"message":"pong"}


@pytest.mark.anyio
async def test_create_machine():
    async with AsyncClient(app=app, base_url='http://test') as ac:
        params = {
            "name":"Test Machine Learning Model",
            "description":"""This is a mock example for testing
            purposes""",
            "hyperparameters":{"channels":3,"out":600,"table":"__test__"},
            "optimizer":"SGD",
            "criterion":"MSE",
            "epochs":15,
            "log_path":"../logs/testmodel.log",
            "dataset_path":"../datasets/",
            "dataset_size":100000,
            "ml_path":"../models/"         
        }
        response = await ac.post("/new_machine", json=params)
        assert response.status_code==200
        data = response.json()
        for k, v in params.items():
            assert data[k]==v


@pytest.mark.anyio
async def test_create_register():
    async with AsyncClient(app=app, base_url='http://test') as ac:
        params = {
            "name":"Test Machine Learning Model",
            "description":"""This is a mock example for testing
            purposes""",
            "hyperparameters":{"channels":3,"out":600,"table":"__test__"},
            "optimizer":"SGD",
            "criterion":"MSE",
            "epochs":15,
            "log_path":"../logs/testmodel.log",
            "dataset_path":"../datasets/",
            "dataset_size":100000,
            "ml_path":"../models/"         
        }
        response = await ac.post("/new_machine", json=params)
        assert response.status_code==200
        data = response.json()
        machine_id = data["id"]
        register_params = {
            "machine_id":machine_id,
            "duration":300.5,
            "timestamp":datetime.utcnow().isoformat(),
            "stage":1,
            "stage_model":"../model/model_01.model",
            "loss":{"e":.5,"n":.1,"s":.6},
            "acc":{"e":.9,"n":.8,"s":.7}
        }
        response = await ac.post("/new_register", json=register_params)
        assert response.status_code==200
        
        data = response.json()
        for k, v in register_params.items():
            assert data[k]==v



@pytest.mark.anyio
async def test_create_machine():
    async with AsyncClient(app=app, base_url='http://test') as ac:
        params = {
            "name":"Test Machine Learning Model",
            "description":"""This is a mock example for testing
            purposes""",
            "hyperparameters":{"channels":3,"out":600,"table":"__test__"},
            "optimizer":"SGD",
            "criterion":"MSE",
            "epochs":15,
            "log_path":"../logs/testmodel.log",
            "dataset_path":"../datasets/",
            "dataset_size":100000,
            "ml_path":"../models/"         
        }
        response = await ac.post("/new_machine", json=params)
        assert response.status_code==200
        data = response.json()
        for k, v in params.items():
            assert data[k]==v


@pytest.mark.anyio
async def test_get_last_register():
    async with AsyncClient(app=app, base_url='http://test') as ac:
        params = {
            "name":"Test Machine Learning Model",
            "description":"""This is a mock example for testing
            purposes""",
            "hyperparameters":{"channels":3,"out":600,"table":"__test__"},
            "optimizer":"SGD",
            "criterion":"MSE",
            "epochs":15,
            "log_path":"../logs/testmodel.log",
            "dataset_path":"../datasets/",
            "dataset_size":100000,
            "ml_path":"../models/"         
        }
        response = await ac.post("/new_machine", json=params)
        assert response.status_code==200
        data = response.json()
        machine_id = data["id"]
        register_params = {
            "machine_id":machine_id,
            "duration":300.5,
            "timestamp":datetime.utcnow().isoformat(),
            "stage":1,
            "stage_model":"../model/model_01.model",
            "loss":{"e":.5,"n":.1,"s":.6},
            "acc":{"e":.9,"n":.8,"s":.7}
        }
        response = await ac.post("/new_register", json=register_params)
        assert response.status_code==200


        register_params = {
            "machine_id":machine_id,
            "duration":290.6,
            "timestamp":datetime.utcnow().isoformat(),
            "stage":2,
            "stage_model":"../model/model_02.model",
            "loss":{"e":.5,"n":.2,"s":.7},
            "acc":{"e":.7,"n":.6,"s":.7}
        }
        response = await ac.post("/new_register", json=register_params)
        assert response.status_code==200


        register_params = {
            "machine_id":machine_id,
            "duration":250.9,
            "timestamp":datetime.utcnow().isoformat(),
            "stage":3,
            "stage_model":"../model/model_03.model",
            "loss":{"e":.1,"n":.1,"s":.1},
            "acc":{"e":.8,"n":.8,"s":.8}
        }
        response = await ac.post("/new_register", json=register_params)
        assert response.status_code==200
        

        response = await ac.get("/last_register", params={"machine_id":machine_id})
        assert response.status_code==200

        data = response.json()
        
        for k, v in register_params.items():
            assert data[0][k]==v



@pytest.mark.anyio
async def test_acc_loss():
    async with AsyncClient(app=app, base_url='http://test') as ac:

        dataset_ref = []
        params = {
            "name":"Test Machine Learning Model",
            "description":"""This is a mock example for testing
            purposes""",
            "hyperparameters":{"channels":3,"out":600,"table":"__test__"},
            "optimizer":"SGD",
            "criterion":"MSE",
            "epochs":15,
            "log_path":"../logs/testmodel.log",
            "dataset_path":"../datasets/",
            "dataset_size":100000,
            "ml_path":"../models/"         
        }
        response = await ac.post("/new_machine", json=params)
        assert response.status_code==200
        data = response.json()
        machine_id = data["id"]
        register_params = {
            "machine_id":machine_id,
            "duration":300.5,
            "timestamp":datetime.utcnow().isoformat(),
            "stage":1,
            "stage_model":"../model/model_01.model",
            "loss":{"e":.5,"n":.1,"s":.6},
            "acc":{"e":.9,"n":.8,"s":.7}
        }
        response = await ac.post("/new_register", json=register_params)
        assert response.status_code==200
        item = {
            "stage":register_params["stage"],
            "ts":register_params["timestamp"],
            **{"loss_e":.5,"loss_n":.1,"loss_s":.6},
            **{"acc_e":.9,"acc_n":.8,"acc_s":.7}
        }
        dataset_ref.append(item)

        register_params = {
            "machine_id":machine_id,
            "duration":290.6,
            "timestamp":datetime.utcnow().isoformat(),
            "stage":2,
            "stage_model":"../model/model_02.model",
            "loss":{"e":.5,"n":.2,"s":.7},
            "acc":{"e":.7,"n":.6,"s":.7}
        }
        response = await ac.post("/new_register", json=register_params)
        assert response.status_code==200
        item = {
            "stage":register_params["stage"],
            "ts":register_params["timestamp"],
            **{"loss_e":.5,"loss_n":.2,"loss_s":.7},
            **{"acc_e":.7,"acc_n":.6,"acc_s":.7}
        }
        dataset_ref.append(item)


        register_params = {
            "machine_id":machine_id,
            "duration":250.9,
            "timestamp":datetime.utcnow().isoformat(),
            "stage":3,
            "stage_model":"../model/model_03.model",
            "loss":{"e":.1,"n":.1,"s":.1},
            "acc":{"e":.8,"n":.8,"s":.8}
        }
        response = await ac.post("/new_register", json=register_params)
        assert response.status_code==200
        item = {
            "stage":register_params["stage"],
            "ts":register_params["timestamp"],
            **{"loss_e":.1,"loss_n":.1,"loss_s":.1},
            **{"acc_e":.8,"acc_n":.8,"acc_s":.8}
        }
        dataset_ref.append(item)
        

        response = await ac.get("/acc_loss", params={"machine_id":machine_id,"stage":3})
        assert response.status_code==200

        data = sorted(response.json()[0], key=lambda x:x["stage"])


        for i,item in enumerate(dataset_ref):
            data_item = data[i]
            assert item==data_item

